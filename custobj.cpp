/*
    CustObjQSett
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/

#include <QSettings>
#include <QDebug>

#include "custobj.h"

custobj::custobj(QWidget *parent)
    : QWidget(parent)
{
    qRegisterMetaType<CustomObject>("CustomObject");
    qRegisterMetaTypeStreamOperators<CustomObject>("CustomObject");

    CustomObject *o = new CustomObject();
    o->setI(1);
    o->setStr("One");
    QSettings *s = new QSettings("CustObj");
    qDebug() << "File: " << s->fileName();
    s->setValue("Class", QVariant::fromValue(*o));
    s->sync();
}

custobj::~custobj()
{
}


int CustomObject::i() const
{
    return m_i;
}

void CustomObject::setI(int i)
{
    m_i = i;
}

QString CustomObject::str() const
{
    return m_str;
}

void CustomObject::setStr(const QString &str)
{
    m_str = str;
}
