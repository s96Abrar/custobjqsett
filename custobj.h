/*
    CustObjQSett
    Copyright (C) 2020  Abrar

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


*/

#ifndef CUSTOBJ_H
#define CUSTOBJ_H

#include <QWidget>

class CustomObject {
public:
    CustomObject() :
        m_i(0), m_str("") { }

    friend QDataStream &operator<<(QDataStream &out, const CustomObject &obj)
    {
        out << obj.m_i << obj.m_str;
        return out;
    }

    friend QDataStream &operator>>(QDataStream &in, CustomObject &obj)
    {
       in >> obj.m_i >> obj.m_str;
       return in;
    }

    int i() const;
    void setI(int i);

    QString str() const;
    void setStr(const QString &str);

private:
    int m_i = 0;
    QString m_str = "";

};
Q_DECLARE_METATYPE(CustomObject)


class custobj : public QWidget
{
    Q_OBJECT

public:
    custobj(QWidget *parent = nullptr);
    ~custobj();
};
#endif // CUSTOBJ_H
